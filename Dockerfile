FROM nginx:alpine
COPY build /usr/share/nginx/html
RUN sed -ie '11itry_files $uri /index.html;' /etc/nginx/conf.d/default.conf

EXPOSE 80