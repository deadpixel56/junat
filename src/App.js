import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { withNamespaces } from 'react-i18next';
import axios from 'axios';

import Home from './views/Home/Home';
import Stations from './views/Stations/Stations';
import Trains from './views/Trains/Trains';
import Train from './views/Train/Train';
import Station from './views/Station/Station';
import Privacy from './views/Privacy/Privacy';
import Team from './views/Team/Team';
import NotFound from './views/404/404';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'leaflet/dist/leaflet.css';
import 'font-awesome/css/font-awesome.css';

import Logo from './Assets/logo.png';

class App extends Component {
    timer = null;
    dateTimer = null;
    timerInterval = 1000 * 15;
    stationsUrl = 'https://rata.digitraffic.fi/api/v1/metadata/stations';
    trainsUrl = 'https://rata.digitraffic.fi/api/v1/live-trains';
    locationUrl = 'https://rata.digitraffic.fi/api/v1/train-locations/latest/';
    hidden = '';
    visibilityChange = '';

    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            stations: {},
            trains: {},
            loadingStations: true,
            loadingTrains: true,
            showCargo: false,
            language: 'fi'
        };

        this.findStation = this.findStation.bind(this);
        this.togglePassenger = this.togglePassenger.bind(this);
        this.handleVisibilityChange = this.handleVisibilityChange.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.changeLanguage = this.changeLanguage.bind(this);
    }

    componentDidMount() {
        window.addEventListener("focus", this.onFocus);
        window.addEventListener("blur", this.onBlur);
        this.onFocus();

        axios.get(this.stationsUrl)
            .then(response => {
                let stations = {};
                response.data.forEach((element) => {
                    element.stationName = element.stationName
                        .replace(' asema', '')
                        .replace(' Asema', '')
                        .replace('_', ' ');
                    stations[element.stationShortCode] = element;
                });
                this.setState({ stations: stations, loadingStations: false });
            });

        if (typeof document.hidden !== "undefined") {
            this.hidden = "hidden";
            this.visibilityChange = "visibilitychange";
        } else if (typeof document.msHidden !== "undefined") {
            this.hidden = "msHidden";
            this.visibilityChange = "msvisibilitychange";
        } else if (typeof document.webkitHidden !== "undefined") {
            this.hidden = "webkitHidden";
            this.visibilityChange = "webkitvisibilitychange";
        }
        document.addEventListener(this.visibilityChange, this.handleVisibilityChange, false);
    }

    changeLanguage(lang) {
        this.props.i18n.changeLanguage(lang);
        this.setState({ language: lang });
    }

    togglePassenger(value) {
        this.setState({ showCargo: !value });
    }

    handleVisibilityChange() {
        if (document[this.hidden]) {
            this.onBlur();
        } else {
            this.onFocus();
        }
    }

    componentWillUnmount() {
        window.removeEventListener(this.visibilityChange, this.handleVisibilityChange);
        this.onBlur();
    }

    onFocus() {
        if (this.timer !== null) {
            return;
        }
        this.updateData();
        this.timer = setInterval(() => { this.updateData() }, this.timerInterval);
        this.dateTimer = setInterval(() => {
            let date = new Date();
            if (date.getMinutes() !== this.state.date.getMinutes()) {
                this.setState({ date: date });
            }
        }, 1000);
    }

    onBlur() {
        if (this.timer === null) {
            return;
        }

        clearInterval(this.timer);
        this.timer = null;
        clearInterval(this.dateTimer);
        this.dateTimer = null;
    }

    findStation(code) {
        return this.state && this.state.stations[code] ? this.state.stations[code] : null;
    }

    updateData() {
        axios.get(this.trainsUrl)
            .then(response => {
                let trains = {};
                response.data.forEach(element => {
                    if (element.runningCurrently && !element.cancelled) {
                        trains[element.trainNumber] = element;
                    }
                });
                this.setState({
                    trains: trains, loadingTrains: false
                });
            });
        axios.get(this.locationUrl)
            .then(response => {
                let locations = [];
                response.data.forEach(loc => {
                    if (!this.state.trains[loc.trainNumber]) {
                        return;
                    }
                    locations[loc.trainNumber] = {
                        train: loc.trainNumber,
                        type: this.state.trains[loc.trainNumber].trainType,
                        latitude: loc.location.coordinates[1],
                        longitude: loc.location.coordinates[0]
                    };
                });
                this.setState({
                    locations: locations
                });
            }).catch(error => console.log(error));
    }

    render() {
        const { t } = this.props;
        return (
            <div className="App">
                <div className="menu">
                    <div className="container">
                        <Link to={'/'}>
                            <img src={Logo} alt="Junat.App" />
                            Junat.App
                        </Link>
                        <a className={'float-right lang' + (this.state.language === 'ru' ? ' selected' : '')} onClick={e => this.changeLanguage('ru')}>RU</a>
                        <a className={'float-right lang' + (this.state.language === 'en' ? ' selected' : '')} onClick={e => this.changeLanguage('en')}>EN</a>
                        <a className={'float-right lang' + (this.state.language === 'fi' ? ' selected' : '')} onClick={e => this.changeLanguage('fi')}>FI</a>
                    </div>
                </div>
                <div className="content">
                    <Switch>
                        <Route exact path='/' render={(props) => <Home {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} locations={this.state.locations} trains={this.state.trains} date={this.state.date} findStation={this.findStation} loading={this.state.loadingStations || this.state.loadingTrains} />} />

                        <Route path='/stations/:code' render={(props) => <Station {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} stations={this.state.stations} findStation={this.findStation} loading={this.state.loadingStations} date={this.state.date} />} />
                        <Route path='/stations' render={(props) => <Stations {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} stations={this.state.stations} trains={this.state.trains.length} loading={this.state.loadingStations} date={this.state.date} />} />
                        <Route path='/trains/:number' render={(props) => <Train {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} locations={this.state.locations} stations={this.state.stations} findStation={this.findStation} trains={this.state.trains} date={this.state.date} />} />
                        <Route path='/trains' render={(props) => <Trains {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} stations={this.state.stations} findStation={this.findStation} trains={this.state.trains} loading={this.state.loadingTrains} date={this.state.date} />} />
                        <Route path='/privacy' render={(props) => <Privacy {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} trains={this.state.trains} date={this.state.date} />} />
                        <Route path='/team' render={(props) => <Team {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} trains={this.state.trains} date={this.state.date} />} />
                        <Route path='/' render={(props) => <NotFound {...props} t={t} togglePassenger={this.togglePassenger} showCargo={this.state.showCargo} trains={this.state.trains} date={this.state.date} findStation={this.findStation} loading={this.state.loadingStations || this.state.loadingTrains} />} />
                    </Switch>
                </div>
                <div className="container">
                    <div className="footer">
                        &copy; Copyright {new Date().getFullYear()} Junat.App
                        <span className="float-right">
                            <Link to={'/privacy'}>{t('privacy.name')}</Link>
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default withNamespaces('translation')(App);
