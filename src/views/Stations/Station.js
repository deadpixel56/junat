import React from 'react';
import { Link } from 'react-router-dom'

export default props => {
    return (
        <Link to={'/stations/' + props.station.stationShortCode} className="card mb-3">
            <div className="card-body p-2">
                <div className="badge badge-primary mr-2">{props.station.stationShortCode}</div>
                {props.station.stationName}
            </div>
        </Link>
    );
};