import React, { Component } from 'react';
import Search from './Search';
import Station from './Station';
import Header from '../../components/header';
import Loader from '../../components/loader';

class Stations extends Component {
    constructor(props) {
        super(props);
        this.state = { filter: '' };

        this.onFilter = this.onFilter.bind(this);
    }

    onFilter(event) {
        this.setState({ filter: event.target.value.toUpperCase() });
    }

    render() {
        document.title = 'Junat | Asemat';
        if (this.props.loading) {
            return <Loader />
        }

        let stations = Object.keys(this.props.stations).map(index => {
            const element = this.props.stations[index];
            if (!element.passengerTraffic) {
                return null;
            }
            if (this.state.filter !== ''
                && !element.stationName.toUpperCase().includes(this.state.filter)
                && !element.stationShortCode.includes(this.state.filter)
            ) {
                return null;
            }
            return <div className="col-md-6 col-lg-4" key={element.stationName}><div><Station station={element} /></div></div>
        }).filter(el => {
            return el != null;
        });
        return (
            <div>
                <Header t={this.props.t} togglePassenger={this.props.togglePassenger} showCargo={this.props.showCargo} title={this.props.t('header_active_trains')} value={this.props.trains} time={this.props.t('header_time')} date={this.props.date} />
                <div className="container">
                    <Search placeholder={this.props.t('search')} value={this.state.filter} change={this.onFilter} />
                    <div className="row">
                        {stations.length === 0 ? <div className="col-12"><div className="alert alert-info">{this.props.t('stations.no-stations')}</div></div> : stations}
                    </div>
                </div>
            </div>
        );
    };
};

export default Stations;