import React from 'react';
import { Link } from 'react-router-dom';

import Header from '../../components/header';

export default props => {
    document.title = 'Junat | 404';
    return (
        <div>
            <Header t={props.t} togglePassenger={props.togglePassenger} showCargo={props.showCargo} title={props.t('header_active_trains')} value={props.trains.length} time={props.t('header_time')} date={props.date} />
            <div class="container">
                <div class="text-center">
                    <div style={{ 'font-size': '25rem', 'line-height': '100%', 'z-index': -1 }}>404</div>
                    <h2 class="text-danger mt-0">{props.t('404.title')}</h2>
                    <p>{props.t('404.description')}</p>
                    <Link to="/" class="btn btn-primary my-3">
                        {props.t('404.back')}
                    </Link>
                </div>
            </div>
        </div>
    );
}