import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Map, Marker, TileLayer } from 'react-leaflet';

import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class TrainMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            zoom: 13,
            follow: true,
            center: null
        }
    }

    render() {
        let markers = (this.props.locations || []).map(loc => {
            let icon = L.divIcon({
                className: 'train-icon',
                html: `<span class="circle">${loc.type}</span>${loc.train}`,
                iconAnchor: [20, 15],
                iconSize: [40, 30],
            });
            return (
                <Marker key={loc.train} position={[loc.latitude, loc.longitude]} onClick={() => this.props.history.push('/trains/' + loc.train)} icon={icon} />
            );
        });
        return (
            <div className="map-container card mb-3">
                <Map bounds={[[59.846373196, 20.6455928891], [70.1641930203, 31.5160921567]]} id="map" attributionControl={false}>
                    <TileLayer
                        url="https://tiles.junat.app/basemap/{z}/{x}/{y}.png"
                    />
                    {markers}
                </Map>
            </div>
        );
    }
};

export default withRouter(TrainMap);