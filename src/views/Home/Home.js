import React from 'react';
import { Link } from 'react-router-dom';

import Train from './Train';
import Map from './Map';
import Loader from '../../components/loader';

export default props => {
    document.title = 'Junat | Home';

    if (props.loading) {
        return <Loader />
    }

    let trains = Object.keys(props.trains).map(index => {
        const element = props.trains[index];
        if (!props.showCargo
            && element.trainCategory !== 'Commuter'
            && element.trainCategory !== 'Long-distance') {
            return null;
        }
        let delayed = false;
        let delay = 0;
        element.timeTableRows.forEach(row => {
            let time = new Date(row.liveEstimateTime ? row.liveEstimateTime : row.actualTime);
            if (time > props.date && row.differenceInMinutes > 0 && !delayed) {
                delayed = true;
                delay = row.differenceInMinutes;
            }
        });
        if (!delayed) {
            return null;
        }
        return <div key={element.trainNumber} className="col-md-6 col-lg-4"><div><Train t={props.t} train={element} delay={delay} findStation={props.findStation} /></div></div>
    });

    return (
        <div>
            <header className="home">
                <div className="divider-shape">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" className="shape-waves" style={{ left: 0, transform: 'rotate3d(0,1,0,180deg)' }}>
                        <path className="shape-fill shape-fill-1" d="M790.5,93.1c-59.3-5.3-116.8-18-192.6-50c-29.6-12.7-76.9-31-100.5-35.9c-23.6-4.9-52.6-7.8-75.5-5.3c-10.2,1.1-22.6,1.4-50.1,7.4c-27.2,6.3-58.2,16.6-79.4,24.7c-41.3,15.9-94.9,21.9-134,22.6C72,58.2,0,25.8,0,25.8V100h1000V65.3c0,0-51.5,19.4-106.2,25.7C839.5,97,814.1,95.2,790.5,93.1z"></path>
                    </svg>
                </div>
                <div className="container">
                    <h2 className="mb-4">{props.t('home.title')}</h2>
                    <div className="row">
                        <div className="col-md-6">
                            <p>
                                {props.t('home.line1')}<br />
                                {props.t('home.line2')}<br />
                                {props.t('home.line3')}<br />
                            </p>
                        </div>
                        <div className="col-md-6 text-center">
                            <div className="row">
                                <div className="col">
                                    <p className="mb-0">{props.t('header_active_trains')}</p>
                                    <h2>{Object.keys(props.trains).length}</h2>
                                </div>
                                <div className="col">
                                    <p className="mb-0">{props.t('header_time')}</p>
                                    <h2>{('0' + props.date.getHours()).substr(-2, 2)}:{('0' + props.date.getMinutes()).substr(-2, 2)}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="custom-control custom-checkbox">
                        <div>
                            <input type="checkbox" className="custom-control-input" id="showCargo" onChange={e => props.togglePassenger(e.target.checked)} checked={!props.showCargo} />
                            <label className="custom-control-label" htmlFor="showCargo">{props.t('header_only_passenger')}</label>
                        </div>
                    </div>
                </div>
            </header>
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <p>{props.t('home.line4')}</p>
                        <p className="mb-5" dangerouslySetInnerHTML={{ __html: props.t('home.line5') }}></p>
                    </div>
                    <div className="col-md-6 text-center">
                        <p className="handwritten">{props.t('home.line6')}</p>
                        <div className="row">
                            <div className="col-md-6 text-center">
                                <Link to={'/trains'} className="btn btn-primary btn-block mb-2">{props.t('trains.link')}</Link>
                            </div>
                            <div className="col-md-6 text-center">
                                <Link to={'/stations'} className="btn btn-primary btn-block mb-5">{props.t('stations.link')}</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <h2 className="mb-3">{props.t('home.delays')}</h2>
                <div className="row">
                    {trains.length === 0 ? <div className="col-12"><div className="alert alert-info">{props.t('home.no-delays')}</div></div> : trains}
                </div>
                <h2 className="mb-3 mt-5">{props.t('home.trains')}</h2>
                <Map
                    t={props.t}
                    locations={props.locations}
                    changeTrain={this.changeTrain}
                />
            </div>
        </div>
    );
};