import React from 'react';

import Header from '../../components/header';

export default props => {
    document.title = 'Junat | Tietosuojakäytäntö';
    return (
        <div>
            <Header t={props.t} togglePassenger={props.togglePassenger} showCargo={props.showCargo} title={props.t('header_active_trains')} value={Object.keys(props.trains).length} time={props.t('header_time')} date={props.date} />
            <div className="container">
                <h2>{props.t('privacy.name')}</h2>
                <p>
                    {
                        props.t('privacy.intro').split("\n").map((item, key) => {
                            return (
                                <span key={key}>
                                    {item}
                                    <br />
                                </span>
                            );
                        })
                    }
                </p>
                <h4 className="mt-4">{props.t('privacy.section1.title')}</h4>
                <p>
                    {
                        props.t('privacy.section1.content').split("\n").map((item, key) => {
                            return (
                                <span key={key}>
                                    {item}
                                    <br />
                                </span>
                            );
                        })
                    }
                </p>
                <h4 className="mt-4">{props.t('privacy.section2.title')}</h4>
                <p className="mb-0">
                    {
                        props.t('privacy.section2.content').split("\n").map((item, key) => {
                            return (
                                <span key={key}>
                                    {item}
                                    <br />
                                </span>
                            );
                        })
                    }
                </p>
            </div>
        </div>
    )
};