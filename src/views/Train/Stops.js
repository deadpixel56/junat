import React from 'react';

import Stop from './Stop';

export default props => {
    let hasPassed = false;
    let passedStops = props.stops.map((element, index) => {
        if (element.departureActual !== null 
            && new Date(element.departureActual) < props.date) {
            hasPassed = true;
            if (!props.showPassed) {
                return null;
            } else {
                return (
                    <Stop 
                        key={index}
                        t={props.t}
                        stop={element}
                        station={props.findStation(element.stationShortCode)}
                        date={props.date} />
                );
            }
        }
        return null;
    });
    let upcomingStops = props.stops.map((element, index) => {
        if (element.departureActual !== null 
            && new Date(element.departureActual) < props.date) {
            return null;
        }
        return (
            <Stop 
                key={index}
                t={props.t}
                stop={element}
                station={props.findStation(element.stationShortCode)}
                date={props.date} />
        );
    });
    let passed = !hasPassed ? null : (
        <button onClick={() => props.togglePassed()} type="button" className="btn btn-primary btn-block mb-3">{props.t(props.showPassed ? 'trains.hide_passed' : 'trains.show_passed')}</button>
    );
    return (
        <div>
            <div>
                {passedStops}
                {passed}
                {upcomingStops}
            </div>
        </div>
    );
}