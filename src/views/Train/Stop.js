import React from 'react';
import { Link } from 'react-router-dom';

export default props => {
    let arrivalActual = new Date(props.stop.arrivalActual);
    let arrivalScheduled = new Date(props.stop.arrivalScheduled);
    let departureActual = new Date(props.stop.departureActual);
    let departureScheduled = new Date(props.stop.departureScheduled);

    arrivalActual.setSeconds(0);
    arrivalActual.setMilliseconds(0);
    arrivalScheduled.setSeconds(0);
    arrivalScheduled.setMilliseconds(0);
    departureActual.setSeconds(0);
    departureActual.setMilliseconds(0);
    departureScheduled.setSeconds(0);
    departureScheduled.setMilliseconds(0);

    let arrivalTime = arrivalActual > arrivalScheduled ?
        (
            <span>
                <span className="text-muted delay">{('0' + arrivalScheduled.getHours()).substr(-2, 2)}:{('0' + arrivalScheduled.getMinutes()).substr(-2, 2)}</span>
                <span>{('0' + arrivalActual.getHours()).substr(-2, 2)}:{('0' + arrivalActual.getMinutes()).substr(-2, 2)}</span>
            </span>
        ) : <span>{('0' + arrivalScheduled.getHours()).substr(-2, 2)}:{('0' + arrivalScheduled.getMinutes()).substr(-2, 2)}</span>
    if (props.stop.arrivalScheduled === null) {
        arrivalTime = null;
    }

    let departureTime = departureActual > departureScheduled ?
        (
            <span>
                <span className="text-muted delay">{('0' + departureScheduled.getHours()).substr(-2, 2)}:{('0' + departureScheduled.getMinutes()).substr(-2, 2)}</span>
                <span>{('0' + departureActual.getHours()).substr(-2, 2)}:{('0' + departureActual.getMinutes()).substr(-2, 2)}</span>
            </span>
        ) : <span>{('0' + departureScheduled.getHours()).substr(-2, 2)}:{('0' + departureScheduled.getMinutes()).substr(-2, 2)}</span>
    if (props.stop.departureScheduled === null) {
        departureTime = null;
    }

    let time = props.stop.cancelled ? props.t('train.stop_cancelled') : (<span>{arrivalTime}{arrivalTime && departureTime ? ' - ' : null}{departureTime}</span>);

    return (
        <Link to={'/stations/' + (props.station === null ? 'N/a' : props.station.stationShortCode)} className={props.stop.departureScheduled !== null && new Date(props.stop.departureActual) < props.date ? 'card mb-3 passed' : 'card mb-3'}>
            <div className="card-body p-2">
                <div className="badge badge-primary mr-2">{props.station === null ? 'N/a' : props.station.stationShortCode}</div>
                {props.station === null ? 'N/a' : props.station.stationName}
                <span className="float-right">
                    {time}
                </span>
                <p className="m-0 text-muted">{props.stop.cancelled ? props.t('train.stop_cancelled') : props.t('trains.track') + ' ' + props.stop.track}</p>
            </div>
        </Link>
    );
}