import React from 'react';

class Composition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            car: 0
        };
    }

    render() {
        let rows = <div className="alert alert-info">{this.props.t('trains.no_composition')}</div>;
        if (this.props.composition.length > 0) {
            if (this.state.car >= this.props.composition.length) {
                this.setState({car: this.props.composition.length - 1});
            } else {
                let element = this.props.composition[this.state.car];
                let buttons = this.props.composition.map((element, index) => {
                    return <button key={index} onClick={() => this.setState({car: index})} type="button" className={'btn btn-block ' + (this.state.car === index ? 'btn-primary' : 'btn-secondary')}>{element.salesNumber}</button>
                });
                rows = (
                    <div className="row no-gutters">
                        <div className="col-1">
                        </div>
                        <div className="col-10 text-center"><img src={`/images/cars/${element.wagonType}.png`} alt={element.wagonType} className="img-fluid" /></div>
                        <div className="col-1">
                            <div className="btn-group-vertical w-100" role="group" aria-label="Button group with nested dropdown">
                                {buttons}
                            </div>
                        </div>
                    </div>
                );
            }
        }
        return (
            <div>
                {rows}
            </div>
        );
    }
}

export default Composition;