import React, { Component } from 'react';
import { Map, Marker, TileLayer } from 'react-leaflet';

import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

class TrainMap extends Component {
    constructor(props) {
       super(props);
        this.state = {
            zoom: 13,
            follow: true,
            center: null
        }
    }

    zoomed(event) {
        this.setState({
            zoom: event.target.getZoom()
        });
    }

    moved(event) {
        this.setState({
            follow: false
        });
    }

    static getDerivedStateFromProps(props, state) {
        return {
            ...state,
            center: state.follow || state.center === null ? props.loc : state.center
        };
    }

    render() {
        let markers = (this.props.locations || []).map(loc => {
            let icon = L.divIcon({
                className: 'train-icon',
                html: `<span class="circle">${loc.type}</span>${loc.train}`,
                iconAnchor: [20, 15],
                iconSize: [40, 30],
            });
            return (
                <Marker key={loc.train} position={[loc.latitude, loc.longitude]} onClick={() => this.props.changeTrain(loc.train)} icon={icon} />
            );
        });
        return this.props.loc === null
            ? <div className="alert alert-info text-center">{this.props.t('trains.no_gps')}</div>
            : (
                <div className="map-container card mb-3">
                    <div className="custom-control custom-checkbox">
                        <div>
                            <input type="checkbox" className="custom-control-input" id="followTrain" onChange={e => this.setState({follow: e.target.checked})} checked={this.state.follow} />
                            <label className="custom-control-label" htmlFor="followTrain">{this.props.t('trains.follow')}</label>
                        </div>
                    </div>
                    <Map center={this.state.center} zoom={this.state.zoom} id="map" attributionControl={false} onZoomEnd={e => this.zoomed(e)} onDragEnd={e => this.moved(e)}>
                        <TileLayer
                            url="https://tiles.junat.app/basemap/{z}/{x}/{y}.png"
                        />
                        {markers}
                    </Map>
                </div>
            );
    }
};

export default TrainMap;