import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Header from '../../components/header';
import Loader from '../../components/loader';
import Map from './Map';
import Stops from './Stops';
import Composition from './Composition';


class Train extends Component {
    timer = null;
    timerInterval = 1000 * 15;
    dataUrl = 'https://rata.digitraffic.fi/api/v1/trains/latest/';
    compositionUrl = 'https://rata.digitraffic.fi/api/v1/compositions/';

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.number,
            train: null,
            composition: [],
            stops: [],
            loading: true,
            showPassed: false,
            tab: 1,
            change: -1
        };

        this.updateData = this.updateData.bind(this);
        this.togglePassed = this.togglePassed.bind(this);
        this.changeTrain = this.changeTrain.bind(this);
    }

    padNumber(num) {
        return ('0' + num).substr(-2, 2);
    }

    formatDate(date) {
        return date.getFullYear() + '-' + this.padNumber(date.getMonth() + 1) + '-' + this.padNumber(date.getDate())
    }

    componentDidMount() {
        this.timer = setInterval(() => this.updateData(), this.timerInterval);
        this.start();
    }

    start() {
        this.updateData();
        axios.get(this.compositionUrl + this.formatDate(this.props.date) + '/' + this.state.id)
            .then(response => {
                if (response.data.errorMessage) {
                    this.setState({ composition: [] });
                } else {
                    let composition = response.data.journeySections[0].wagons.map(element => {
                        return {
                            ...element,
                            salesNumber: element.salesNumber === 0 ? element.location : element.salesNumber
                        }
                    });
                    if (composition.length === 0) {
                        response.data.journeySections[0].locomotives.forEach(loco => {
                            composition.push({
                                wagonType: loco.locomotiveType,
                                salesNumber: loco.location
                            });
                        });
                    }
                    composition.sort((a, b) => {
                        if (a.salesNumber < b.salesNumber)
                            return 1;
                        if (a.salesNumber > b.salesNumber)
                            return -1;
                        return 0;
                    });
                    this.setState({ composition: composition });
                }
            });
    }

    componentWillUnmount() {
        clearInterval(this.timer);
        this.timer = null;
    }

    updateData() {
        axios.get(this.dataUrl + this.state.id)
            .then(response => {
                let stops = [];
                response.data[0].timeTableRows.forEach(element => {
                    if (!element.trainStopping) {
                        return;
                    }
                    if (element.commercialStop !== undefined && !element.commercialStop) {
                        return;
                    }
                    let found = false;
                    for (let i = 0; i < stops.length; i++) {
                        if (stops[i].stationShortCode === element.stationShortCode && (isNaN(stops[i].track) || stops[i].track === parseInt(element.commercialTrack, 10))) {
                            if (element.type === 'DEPARTURE') {
                                if (isNaN(stops[i].track)) {
                                    stops[i].track = parseInt(element.commercialTrack, 10);
                                }
                                stops[i].cancelled = element.cancelled;
                                stops[i].departureScheduled = element.scheduledTime;
                                stops[i].departureActual = element.liveEstimateTime ? element.liveEstimateTime : element.actualTime;
                            }
                            if (element.type === 'ARRIVAL') {
                                stops[i].arrivalScheduled = element.scheduledTime;
                                stops[i].arrivalActual = element.liveEstimateTime ? element.liveEstimateTime : element.actualTime;
                            }
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        if (element.type === 'DEPARTURE') {
                            stops.push({
                                stationShortCode: element.stationShortCode,
                                arrivalScheduled: null,
                                arrivalActual: null,
                                departureScheduled: element.scheduledTime,
                                departureActual: element.liveEstimateTime ? element.liveEstimateTime : element.actualTime,
                                cancelled: element.cancelled,
                                track: parseInt(element.commercialTrack, 10)
                            });
                        }
                        if (element.type === 'ARRIVAL') {
                            stops.push({
                                stationShortCode: element.stationShortCode,
                                arrivalScheduled: element.scheduledTime,
                                arrivalActual: element.liveEstimateTime ? element.liveEstimateTime : element.actualTime,
                                departureScheduled: null,
                                departureActual: null,
                                cancelled: element.cancelled,
                                track: parseInt(element.commercialTrack, 10)
                            });
                        }
                    }
                });
                this.setState({ train: response.data[0], loading: false, stops: stops });
            });
    }

    getLocation() {
        let loc = null;
        if (this.props.locations && this.props.locations[this.state.id]) {
            loc = [this.props.locations[this.state.id].latitude, this.props.locations[this.state.id].longitude];
        } else {
            if (this.state.stops.length === 0) {
                loc = [0, 0];
            } else if (this.props.date < new Date(this.state.stops[0].departureScheduled)) {
                let station = this.props.findStation(this.state.stops[0].stationShortCode);
                if (station === null) {
                    loc = [0, 0];
                } else {
                    loc = [station.latitude, station.longitude];
                }
            } else if (this.props.date > new Date(this.state.stops[this.state.stops.length - 1].arrivalScheduled)) {
                let station = this.props.findStation(this.state.stops[this.state.stops.length - 1].stationShortCode);
                if (station === null) {
                    loc = [0, 0];
                } else {
                    loc = [station.latitude, station.longitude];
                }
            }
        }
        return loc;
    }

    changeTrain(id) {
        if (id === this.state.id) {
            return;
        }
        
        this.setState({id: id});
        this.start();
        this.props.history.push('/trains/' + id);
    }

    togglePassed() {
        this.setState({ showPassed: !this.state.showPassed })
    }

    render() {
        if (this.state.loading) {
            return <Loader />
        }

        let name = this.state.train.trainCategory === 'Commuter' ? this.state.train.commuterLineID : this.state.train.trainType;
        if (this.state.stops.length > 0) {
            document.title = 'Junat | ' + name + this.state.train.trainNumber + ' (' + this.state.stops[0].stationShortCode + ' - ' + this.state.stops[this.state.stops.length - 1].stationShortCode + ')';
        } else {
            document.title = 'Junat | ' + name + this.state.train.trainNumber;
        }

        return (
            <div>
                <Header t={this.props.t} togglePassenger={this.props.togglePassenger} showCargo={this.props.showCargo} title={this.props.t('header_train')} value={name + this.state.train.trainNumber} time={this.props.t('header_time')} date={this.props.date} />
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 mb-3">
                            <Map
                                t={this.props.t}
                                loc={this.getLocation()}
                                locations={this.props.locations}
                                changeTrain={this.changeTrain}
                            />
                            <Link className="btn btn-primary btn-block" to={'/trains'}>{this.props.t('trains.back')}</Link>
                        </div>
                        <div className="col-lg-5">
                            <ul className="nav nav-tabs text-center mb-3 d-flex">
                                <li className="nav-item w-50">
                                    <a onClick={() => this.setState({ tab: 1 })} className={'nav-link' + (this.state.tab === 1 ? ' active' : '')}>{this.props.t('trains.view_stops')}</a>
                                </li>
                                <li className="nav-item w-50">
                                    <a onClick={() => this.setState({ tab: 2 })} className={'nav-link' + (this.state.tab === 2 ? ' active' : '')}>{this.props.t('trains.view_composition')}</a>
                                </li>
                            </ul>
                            {this.state.tab === 1 ? <Stops
                                t={this.props.t}
                                stops={this.state.stops}
                                togglePassed={this.togglePassed}
                                showPassed={this.state.showPassed}
                                findStation={this.props.findStation}
                                date={this.props.date}
                            /> : <Composition t={this.props.t} date={this.props.date} composition={this.state.composition} />}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default Train;