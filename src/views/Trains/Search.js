import React from 'react';

export default props => {
    return (
        <div className="filter mb-3">
            <input type="text" value={props.value} onChange={props.change} className="form-control" placeholder={props.placeholder} />
        </div>
    );
};