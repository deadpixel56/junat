import React from 'react';
import { Link } from 'react-router-dom';

export default props => {
    let name = props.train.trainCategory === 'Commuter' ? props.train.commuterLineID : props.train.trainType;

    let origin = props.train.timeTableRows[0];
    let originStation = props.findStation(origin.stationShortCode);
    let originScheduledTime = new Date(origin.scheduledTime);
    originScheduledTime.setSeconds(0);
    originScheduledTime.setMilliseconds(0);
    let originActualTime = new Date(origin.actualTime);
    originActualTime.setSeconds(0);
    originActualTime.setMilliseconds(0);
    let originTime = originActualTime > originScheduledTime ?
        (
            <span>
                <span className="text-muted delay">{('0' + originScheduledTime.getHours()).substr(-2, 2)}:{('0' + originScheduledTime.getMinutes()).substr(-2, 2)}</span>
                <span>{('0' + originActualTime.getHours()).substr(-2, 2)}:{('0' + originActualTime.getMinutes()).substr(-2, 2)}</span>
            </span>
        ) : <span>{('0' + originScheduledTime.getHours()).substr(-2, 2)}:{('0' + originScheduledTime.getMinutes()).substr(-2, 2)}</span>

    let destination = props.train.timeTableRows[props.train.timeTableRows.length - 1];
    let destinationStation = props.findStation(destination.stationShortCode);
    let destinationScheduledTime = new Date(destination.scheduledTime);
    destinationScheduledTime.setSeconds(0);
    destinationScheduledTime.setMilliseconds(0);
    let destinationActualTime = new Date(destination.liveEstimateTime ? destination.liveEstimateTime : destination.actualTime);
    destinationActualTime.setSeconds(0);
    destinationActualTime.setMilliseconds(0);
    let destinationTime = destinationActualTime > destinationScheduledTime ?
        (
            <span>
                <span className="text-muted delay">{('0' + destinationScheduledTime.getHours()).substr(-2, 2)}:{('0' + destinationScheduledTime.getMinutes()).substr(-2, 2)}</span>
                <span>{('0' + destinationActualTime.getHours()).substr(-2, 2)}:{('0' + destinationActualTime.getMinutes()).substr(-2, 2)}</span>
            </span>
        ) : <span>{('0' + destinationScheduledTime.getHours()).substr(-2, 2)}:{('0' + destinationScheduledTime.getMinutes()).substr(-2, 2)}</span>

    return (
        <Link to={'/trains/' + props.train.trainNumber} className="card mb-3">
            <div className="card-body p-2">
                <div className="badge badge-primary mr-2">{name}</div>
                {name + props.train.trainNumber}
                <span className="float-right">
                    {originTime} - {destinationTime}
                </span>
                <p className="text-muted m-0">{originStation ? originStation.stationName : 'N/a'} &gt; {destinationStation ? destinationStation.stationName : 'N/a'}</p>
            </div>
        </Link>
    );
};