import React, { Component } from 'react';
import Search from './Search';
import Train from './Train';
import Header from '../../components/header';
import Loader from '../../components/loader';

class Trains extends Component {
    constructor(props) {
        super(props);
        this.state = { filter: '' };

        this.onFilter = this.onFilter.bind(this);
    }

    onFilter(event) {
        this.setState({ filter: event.target.value.toUpperCase() });
    }

    render() {
        document.title = 'Junat | Junat';
        if (this.props.loading) {
            return <Loader />
        }

        let trains = Object.keys(this.props.trains).map(index => {
            const element = this.props.trains[index];
            if (!this.props.showCargo 
                && element.trainCategory !== 'Commuter'
                && element.trainCategory !== 'Long-distance') {
                    return null;
                }
            let name = (element.trainCategory === 'Commuter' ? element.commuterLineID : element.trainType) + element.trainNumber;
            if (this.state.filter !== '') {
                let found = false;

                if (name.toUpperCase().includes(this.state.filter)) {
                    found = true;
                } else {
                    for (let i = 0; i < element.timeTableRows.length; i++) {
                        if (element.timeTableRows[i].stationShortCode.includes(this.state.filter)) {
                            found = true;
                            break;
                        } else {
                            let station = this.props.findStation(element.timeTableRows[i].stationShortCode);
                            if (station.stationName.toUpperCase().includes(this.state.filter)) {
                                found = true;
                                break;
                            }
                        }
                    }
                }

                if (!found) {
                    return null;
                }
            }
            return <div key={element.trainNumber} className="col-md-6 col-lg-4"><div><Train train={element} findStation={this.props.findStation} /></div></div>
        }).filter(el => {
            return el != null;
        });
        return (
            <div>
                <Header t={this.props.t} togglePassenger={this.props.togglePassenger} showCargo={this.props.showCargo} title={this.props.t('header_active_trains')} value={Object.keys(this.props.trains).length} time={this.props.t('header_time')} date={this.props.date} />
                <div className="container">
                    <Search placeholder={this.props.t('search')} value={this.state.filter} change={this.onFilter} />
                    <div className="row">
                        {trains.length === 0 ? <div className="col-12"><div className="alert alert-info">{this.props.t('trains.no-trains')}</div></div> : trains}
                    </div>
                </div>
            </div>
        );
    };
};

export default Trains;