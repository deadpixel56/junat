import React from 'react';

export default props => {
    return (
        <div className="card team">
            <div className="row">
                <div className="col-md-5 p-0 image-background" style={{ 'background-image': 'url(' + props.image + ')' }}></div>
                <div className="col-md-7 py-3">
                    <h6>{props.name}</h6>
                    <p className="mt-0 accent mb-5">{props.role}</p>
                    <blockquote className="team-quote pt-1">
                        <p className="italic pl-4">{props.quote}</p>
                    </blockquote>
                </div>
            </div>
        </div>
    );
}