import React from 'react';

import Header from '../../components/header';
import Member from './Member';

export default props => {
    let team = [
        {
            name: 'Dave Flanagan',
            role: 'Primary Developer',
            image: 'https://www.nolwenture.com/assets/photo-dave.6e9731623f46d668685d0b86310bf777.png',
            quote: ''
        },
        {
            name: 'Adrien Hustache',
            role: 'Motivator & Ideas Man',
            image: 'https://www.nolwenture.com/assets/photo-adrien.d866a8dc8c9cfce94212d1300a563ab2.png',
            quote: ''
        }
    ];
    team = team.map(item => {
        return (
            <div className="col-md-6">
                <Member {...item} />
            </div>
        );
    })
    return (
        <div>
            <Header t={props.t} togglePassenger={props.togglePassenger} showCargo={props.showCargo} title={props.t('header_active_trains')} value={Object.keys(props.trains).length} time={props.t('header_time')} date={props.date} />
            <div className="container">
                <div className="row">
                    {team}
                </div>
            </div>
        </div>
    );
};