import React from 'react';
import { Link } from 'react-router-dom';

export default props => {
    let name = props.train.trainCategory === 'Commuter' ? props.train.commuterLineID : props.train.trainType;
    let destinationScheduledTime = new Date(props.train.scheduledTime);
    destinationScheduledTime.setSeconds(0);
    destinationScheduledTime.setMilliseconds(0);
    let destinationActualTime = new Date(props.train.liveEstimateTime ? props.train.liveEstimateTime : props.train.actualTime);
    destinationActualTime.setSeconds(0);
    destinationActualTime.setMilliseconds(0);
    let time = destinationActualTime > destinationScheduledTime ?
        (
            <span>
                <span className="text-muted delay">{('0' + destinationScheduledTime.getHours()).substr(-2, 2)}:{('0' + destinationScheduledTime.getMinutes()).substr(-2, 2)}</span>
                <span>{('0' + destinationActualTime.getHours()).substr(-2, 2)}:{('0' + destinationActualTime.getMinutes()).substr(-2, 2)}</span>
            </span>
        ) : <span>{('0' + destinationScheduledTime.getHours()).substr(-2, 2)}:{('0' + destinationScheduledTime.getMinutes()).substr(-2, 2)}</span>
    return (
        <Link to={'/trains/' + props.train.trainNumber} className="card mb-3">
            <div className="card-body p-2">
                <div className="badge badge-primary mr-2">{name}</div>
                {name + props.train.trainNumber}
                <span className="float-right">
                    {time}
                </span>
                <div className="text-muted">
                    <span>{props.t('trains.track')} {props.train.track}</span>
                    <span className="float-right">
                        {props.train.destination === null ? 'N/a' : props.train.destination.stationName}
                    </span>
                </div>
            </div>
        </Link>
    );
}