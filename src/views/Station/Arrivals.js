import React from 'react';

import Train from './Train';

export default props => {
    let trains = props.arrivals.map((element, index) => {
        if (!props.showCargo 
            && element.trainCategory !== 'Commuter'
            && element.trainCategory !== 'Long-distance') {
                return null;
            }
        return (
            <Train
                t={props.t} 
                key={index}
                train={element} />
        );
    }).filter(el => {
        return el != null;
    });
    return (
        <div>
            {trains.length === 0 ? <div className="alert alert-info">{props.t('stations.no-arrivals')}</div> : trains}
        </div>
    );
}