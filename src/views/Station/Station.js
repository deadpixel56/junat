import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Map from './Map';
import Arrivals from './Arrivals';
import Departures from './Departures';
import Header from '../../components/header';
import Loader from '../../components/loader';

class Station extends Component {
    timer = null;
    timerInterval = 1000 * 15;
    dataUrl = 'https://rata.digitraffic.fi/api/v1/live-trains/station/';
    dataParams = '?arriving_trains=10&departing_trains=10';

    constructor(props) {
        super(props);
        this.state = {
            tab: 1,
            loading: true,
            arrivals: [],
            departures: []
        };
    }

    componentDidMount() {
        setTimeout(() => this.updateData(), 200);
        this.timer = setInterval(() => this.updateData(), this.timerInterval);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
        this.timer = null;
    }

    updateData() {
        axios.get(this.dataUrl + this.props.match.params.code + this.dataParams)
            .then(response => {
                let arrivals = [];
                let departures = [];
                let day = 86400000;
                response.data.forEach(train => {
                    train.timeTableRows.forEach(row => {
                        if (!row.trainStopping) {
                            return;
                        }
                        if (row.commercialStop !== undefined && !row.commercialStop) {
                            return;
                        }

                        if (row.stationShortCode === this.props.match.params.code) {
                            if ((new Date(row.scheduledTime).getTime() / day) - (this.props.date.getTime() / day) > 1) {
                                return;
                            }
                            if (row.type === 'DEPARTURE') {
                                departures.push({
                                    trainNumber: train.trainNumber,
                                    trainType: train.trainType,
                                    trainCategory: train.trainCategory,
                                    commuterLineID: train.commuterLineID,
                                    scheduledTime: row.scheduledTime,
                                    actualTime: row.actualTime,
                                    liveEstimateTime: row.liveEstimateTime,
                                    track: parseInt(row.commercialTrack, 10),
                                    destination: this.props.findStation(train.timeTableRows[train.timeTableRows.length - 1].stationShortCode)
                                });
                            } else {
                                arrivals.push({
                                    trainNumber: train.trainNumber,
                                    trainType: train.trainType,
                                    trainCategory: train.trainCategory,
                                    commuterLineID: train.commuterLineID,
                                    scheduledTime: row.scheduledTime,
                                    actualTime: row.actualTime,
                                    liveEstimateTime: row.liveEstimateTime,
                                    track: parseInt(row.commercialTrack, 10),
                                    destination: this.props.findStation(train.timeTableRows[0].stationShortCode)
                                });
                            }
                        }
                    });
                });
                this.setState({
                    loading: false,
                    arrivals: arrivals.sort((e1, e2) => {
                        let d1 = new Date(e1.scheduledTime);
                        let d2 = new Date(e2.scheduledTime);
                        return d1 > d2 ? 1 : -1;
                    }),
                    departures: departures.sort((e1, e2) => {
                        let d1 = new Date(e1.scheduledTime);
                        let d2 = new Date(e2.scheduledTime);
                        return d1 > d2 ? 1 : -1;
                    })
                });
            });
    }

    render() {
        if (this.props.loading || this.state.loading) {
            return <Loader />
        }

        let station = this.props.findStation(this.props.match.params.code);
        if (station === null) {
            return (<div></div>);
        }
        let loc = [
            station.latitude,
            station.longitude
        ];

        document.title = 'Junat | ' + station.stationName + '(' + this.props.match.params.code + ')';

        return (
            <div className="station">
                <Header t={this.props.t} togglePassenger={this.props.togglePassenger} showCargo={this.props.showCargo} title={this.props.t('header_station')} value={station.stationName} time={this.props.t('header_time')} date={this.props.date} />
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 mb-3">
                            <Map location={loc} />
                            <Link className="btn btn-primary btn-block" to={'/stations'}>{this.props.t('trains.back')}</Link>
                        </div>
                        <div className="col-lg-5">
                            <ul className="nav nav-tabs text-center mb-3 d-flex">
                                <li className="nav-item w-50">
                                    <a onClick={() => this.setState({ tab: 1 })} className={'nav-link' + (this.state.tab === 1 ? ' active' : '')}>{this.props.t('stations.view_arrivals')}</a>
                                </li>
                                <li className="nav-item w-50">
                                    <a onClick={() => this.setState({ tab: 2 })} className={'nav-link' + (this.state.tab === 2 ? ' active' : '')}>{this.props.t('stations.view_departures')}</a>
                                </li>
                            </ul>
                            {this.state.tab === 1 ? <Arrivals showCargo={this.props.showCargo} t={this.props.t} arrivals={this.state.arrivals} /> : <Departures showCargo={this.props.showCargo} t={this.props.t} departures={this.state.departures} />}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default Station;