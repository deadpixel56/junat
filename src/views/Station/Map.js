import React from 'react';
import { Map, Marker, TileLayer } from 'react-leaflet';

import L from 'leaflet';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

export default props => {
    return (
        <div className="map-container card mb-3">
            <Map center={props.location} zoom={13} id="map" attributionControl={false}>
                <TileLayer
                    url="https://tiles.junat.app/basemap/{z}/{x}/{y}.png"
                    apikey="97bfa65b71aa4edf9634d07ec35fc4af"
                />
                <Marker position={props.location} interactive={false} />
            </Map>
        </div>
    );
}