import React from 'react';

export default props => {
    return (
        <div>
            <header className="">
                <div className="divider-shape">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" className="shape-waves" style={{ left: 0, transform: 'rotate3d(0,1,0,180deg)' }}>
                        <path className="shape-fill shape-fill-1" d="M790.5,93.1c-59.3-5.3-116.8-18-192.6-50c-29.6-12.7-76.9-31-100.5-35.9c-23.6-4.9-52.6-7.8-75.5-5.3c-10.2,1.1-22.6,1.4-50.1,7.4c-27.2,6.3-58.2,16.6-79.4,24.7c-41.3,15.9-94.9,21.9-134,22.6C72,58.2,0,25.8,0,25.8V100h1000V65.3c0,0-51.5,19.4-106.2,25.7C839.5,97,814.1,95.2,790.5,93.1z"></path>
                    </svg>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="row text-center text-md-left">
                                <div className="col">
                                    <p className="mb-0">{props.title}</p>
                                    <h2>{props.value}</h2>
                                </div>
                                <div className="col">
                                    <p className="mb-0">{props.time}</p>
                                    <h2>{('0' + props.date.getHours()).substr(-2, 2)}:{('0' + props.date.getMinutes()).substr(-2, 2)}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="custom-control custom-checkbox">
                        <div>
                            <input type="checkbox" className="custom-control-input" id="showCargo" onChange={e => props.togglePassenger(e.target.checked)} checked={!props.showCargo} />
                            <label className="custom-control-label" htmlFor="showCargo">{props.t('header_only_passenger')}</label>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    );
};